const express = require('express');
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./database.sqlite');
const cors = require('cors'); // Importa cors
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const app = express();
const port = 8080;
const SECRET_KEY = 'hide-key';

const databaseFile = './database.sqlite';

const Sentry = require("@sentry/node");
Sentry.init({
  dsn: "https://68d4c2c5f1464e6c9c028d192241751d@o305110.ingest.sentry.io/4505329190305792",
  tracesSampleRate: 1.0,
});

const transaction = Sentry.startTransaction({
  op: "test",
  name: "My First Test Transaction",
});

setTimeout(() => {
  try {
    foo();
  } catch (e) {
    Sentry.captureException(e);
  } finally {
    transaction.finish();
  }
}, 99);

function addUser(username, password) {
  bcrypt.hash(password, 10, (err, hash) => {
    if (err) {
      console.error(err);
      return;
    }

    db.run(
      'INSERT INTO users (username, password) VALUES (?, ?)',
      [username, hash],
      (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log(`User "${username}" added to the database.`);
        }
      }
    );
  });
}

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(401).json({ message: 'No token provided' });
  }

  jwt.verify(token, SECRET_KEY, (err, user) => {
    if (err) {
      return res.status(403).json({ message: 'Invalid token' });
    }

    req.user = user;
    next();
  });
}

db.run(
  `CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL
  )`,
  (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log('Users table created successfully');
      db.get('SELECT * FROM users WHERE username = ?', ['test'], (err, row) => {
        if (err) {
          console.error(err);
          return;
        }
      
        if (!row) {
          addUser('test', 'test');
        } else {
          console.log('User "test" already exists.');
        }
      });
    }
  }
);

app.use(cors()); // Habilita CORS para todas las rutas
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello, World DEMO!');
});
app.post('/login', (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({
      message: 'Both username and password fields are required.',
    });
  }

  db.get('SELECT * FROM users WHERE username = ?', [username], async (err, row) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Internal server error' });
    }

    if (!row) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }
    const passwordsMatch = await bcrypt.compare(password, row.password);

    if (!passwordsMatch) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }

    const token = jwt.sign({ id: row.id, username: row.username }, SECRET_KEY, {
      expiresIn: '1h',
    });

    res.status(200).json({
      message: 'Login successful',
      token,
    });
  });
});

app.get('/user', authenticateToken, (req, res) => {
  const userId = req.user.id;

  db.get('SELECT * FROM users WHERE id = ?', [userId], (err, row) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Internal server error' });
    }

    if (!row) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({
      id: row.id,
      username: row.username,
    });
  });
});

app.post('/register', (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({
      message: 'Both username and password fields are required.',
    });
  }

  db.get('SELECT * FROM users WHERE username = ?', [username], (err, row) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Internal server error' });
    }

    if (row) {
      return res.status(409).json({ message: 'Username already taken' });
    }

    addUser(username, password);
    res.status(201).json({ message: 'User registered successfully' });
  });
});

fs.access(databaseFile, fs.constants.F_OK, (err) => {
  if (err) {
    console.log('Database file not found. Creating a new one...');
    fs.writeFileSync(databaseFile, '');
    console.log('Database file created.');
  } else {
    console.log('Database file found.');
  }

  app.listen(port, () => {
    console.log(`Server is listening at http://localhost:${port}`);
  });
});



